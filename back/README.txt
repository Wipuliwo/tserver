Command to run postgres container on docker:
docker run --rm   --name pg-docker -e POSTGRES_PASSWORD=docker -d -p 5432:5432 postgres

add to intelij vm to change profile:
-Dspring.profiles.active=test

Command to initialize keycloak on docker:
docker run -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -p 8081:8081 jboss/keycloak
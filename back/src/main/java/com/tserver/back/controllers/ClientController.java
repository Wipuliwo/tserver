package com.tserver.back.controllers;

import com.tserver.back.model.Client;
import com.tserver.back.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import java.security.Principal;

@RestController
@RequestMapping("/api")
@EnableSwagger2
public class ClientController extends TserverAbstractController {

	@Autowired
	ClientService clientService;

	@GetMapping(path = "/client/")
	public ResponseEntity<Client> getClient(Principal principal) {
		try {
			return ResponseEntity.ok(clientService.getClientByUsername(principal.getName()));
		} catch (Exception ex) {
			return handleExceptions(ex);
		}
	}

    @PutMapping(path = "/client/")
    public ResponseEntity<Client> putClient(Principal principal , @RequestBody Client client) {
        try {
            return ResponseEntity.ok(clientService.editClientData(client, principal.getName()));
        } catch (Exception ex) {
            return handleExceptions(ex);
        }
    }
}

package com.tserver.back.controllers;

import com.tserver.back.model.UserDetail;
import com.tserver.back.security.JwtUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Controller
@EnableSwagger2
public class JwtController {

    @GetMapping(path = "/auth")
    public ResponseEntity<String> getAuthToken(@AuthenticationPrincipal UserDetail userDetail){
        String token = JwtUtils.getJwtToken(userDetail);
        return ResponseEntity.ok(token);
    }



}

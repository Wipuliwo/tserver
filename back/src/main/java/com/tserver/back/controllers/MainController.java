package com.tserver.back.controllers;

import com.tserver.back.model.Client;
import com.tserver.back.model.RegisterResponse;
import com.tserver.back.repository.ClientRepository;
import com.tserver.back.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@EnableSwagger2
public class MainController {

    @Autowired
    RegistrationService registrationService;

    @Autowired
    ClientRepository clientRepository;

    @GetMapping(path = "/test")
    public ResponseEntity<String> getResponse(){

        return ResponseEntity.ok("The server is Up and running");
    }

    @PostMapping("/register")
    public ResponseEntity registerClient(HttpServletRequest request, @RequestBody Client client){

        RegisterResponse registerResponse = registrationService.registerClient(client);

        return new ResponseEntity(registerResponse.getBody(), registerResponse.getHttpStatus());
    }

}

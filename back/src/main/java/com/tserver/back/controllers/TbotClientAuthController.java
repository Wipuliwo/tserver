package com.tserver.back.controllers;

import com.tserver.back.model.UserDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Controller
@RequestMapping("/api")
@EnableSwagger2
public class TbotClientAuthController {

    @GetMapping(path = "/tbot")
    public ResponseEntity<Boolean> getTbot(@AuthenticationPrincipal UserDetail userDetail){
        return userDetail.isAccountNonExpired() ? ResponseEntity.ok(Boolean.TRUE) : ResponseEntity.ok(Boolean.FALSE);
    }

}

package com.tserver.back.controllers;

import com.tserver.back.exceptions.FieldAlreadyExistsException;
import com.tserver.back.exceptions.InvalidFieldException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.SQLException;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

public abstract class TserverAbstractController {
    protected ResponseEntity handleExceptions(Exception ex){
        if(ex instanceof NoSuchElementException){ return new ResponseEntity<String>(HttpStatus.NOT_FOUND); }
        if(ex instanceof FieldAlreadyExistsException){ return new ResponseEntity(HttpStatus.CONFLICT); }
        if(ex instanceof SQLException){return new ResponseEntity(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);}
        ex.printStackTrace();
        Logger.getAnonymousLogger().warning(ex.getMessage());
        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

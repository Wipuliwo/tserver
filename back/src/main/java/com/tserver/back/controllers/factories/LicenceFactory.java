package com.tserver.back.controllers.factories;

import com.tserver.back.model.Licence;

import java.time.LocalDate;

public class LicenceFactory implements LicenceFactoryInterface {
    
    private static LicenceFactory licenceFactory;
    
    private LicenceFactory(){}
    
    public static LicenceFactory getInstance(){
        if(licenceFactory == null){
            licenceFactory =new LicenceFactory();
        }
        
        return licenceFactory;
    }

    @Override
    public Licence getLicense(Long clientId) {
       Licence licence = new Licence();
       licence.setCliId(clientId);
       licence.setEmission_date(LocalDate.now());

       return licence;
    }
    
}

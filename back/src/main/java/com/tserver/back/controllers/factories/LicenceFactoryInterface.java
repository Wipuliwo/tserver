package com.tserver.back.controllers.factories;

import com.tserver.back.model.Licence;
import org.springframework.stereotype.Component;

public interface LicenceFactoryInterface {

    Licence getLicense(Long clientId);
}

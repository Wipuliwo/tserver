package com.tserver.back.exceptions;

public class FieldAlreadyExistsException extends Exception {

    public FieldAlreadyExistsException(String message) {
        super(message);
    }
}

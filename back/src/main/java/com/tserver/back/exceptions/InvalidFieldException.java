package com.tserver.back.exceptions;

public class InvalidFieldException extends Exception{
    public InvalidFieldException() {
        super("Tried to update with invalid field");
    }
}

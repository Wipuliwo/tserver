package com.tserver.back.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@org.hibernate.annotations.Entity(dynamicUpdate = true)
public class Client {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long cli_id;

    private String first_name;
    private String last_name;
    private String username;
    private String password;
    private String email;
    private boolean emailv;
    private String verification_code;
    private String address;
    private String city;
    private String zip_code;
    private String country;
    private LocalDate creation_date;

    @Override
    public String toString() {
        return "Client{" +
                "cli_id=" + this.cli_id +
                ", first_name='" + this.first_name + '\'' +
                ", last_name='" + this.last_name + '\'' +
                ", username='" + this.username + '\'' +
                ", password='" + this.password + '\'' +
                ", email='" + this.email + '\'' +
                ", emailv=" + this.emailv +
                ", verification_code='" + this.verification_code + '\'' +
                ", address='" + this.address + '\'' +
                ", city='" + this.city + '\'' +
                ", zip_code='" + this.zip_code + '\'' +
                ", country='" + this.country + '\'' +
                ", creation_date=" + this.creation_date +
                '}';
    }

    public Long getCli_id() {
        return this.cli_id;
    }

    public void setCli_id(final Long cli_id) {
        this.cli_id = cli_id;
    }

    public String getFirst_name() {
        return this.first_name;
    }

    public void setFirst_name(final String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return this.last_name;
    }

    public void setLast_name(final String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public boolean getEmailv() {
        return this.emailv;
    }

    public void setEmailv(final boolean emailv) {
        this.emailv = emailv;
    }

    public String getVerification_code() {
        return this.verification_code;
    }

    public void setVerification_code(final String verification_code) {
        this.verification_code = verification_code;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getZip_code() {
        return this.zip_code;
    }

    public void setZip_code(final String zip_code) {
        this.zip_code = zip_code;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    public LocalDate getCreation_date() {
        return this.creation_date;
    }

    public void setCreation_date(final LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(cli_id, client.cli_id) &&
                Objects.equals(first_name, client.first_name) &&
                Objects.equals(last_name, client.last_name) &&
                Objects.equals(username, client.username) &&
                Objects.equals(password, client.password) &&
                Objects.equals(email, client.email) &&
                Objects.equals(emailv, client.emailv) &&
                Objects.equals(verification_code, client.verification_code) &&
                Objects.equals(address, client.address) &&
                Objects.equals(city, client.city) &&
                Objects.equals(zip_code, client.zip_code) &&
                Objects.equals(country, client.country) &&
                Objects.equals(creation_date, client.creation_date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cli_id, first_name, last_name, username, password, email, emailv, verification_code, address, city, zip_code, country, creation_date);
    }
}

package com.tserver.back.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class Licence {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long lic_id;

    private Integer sale_id;
    private Integer licence_key;
    private LocalDate emission_date;
    private LocalDate end_date;
    @Column(name = "cli_id", nullable = false)
    private Long cliId;

    public Long getLic_id() {
        return lic_id;
    }

    public void setLic_id(Long lic_id) {
        this.lic_id = lic_id;
    }

    public Integer getSale_id() {
        return sale_id;
    }

    public void setSale_id(Integer sale_id) {
        this.sale_id = sale_id;
    }

    public Integer getLicence_key() {
        return licence_key;
    }

    public void setLicence_key(Integer licence_key) {
        this.licence_key = licence_key;
    }

    public LocalDate getEmission_date() {
        return emission_date;
    }

    public void setEmission_date(LocalDate emission_date) {
        this.emission_date = emission_date;
    }

    public LocalDate getEnd_date() {
        return end_date;
    }

    public void setEnd_date(LocalDate end_date) {
        this.end_date = end_date;
    }

    public Long getCliId() {
        return cliId;
    }

    public void setCliId(Long cliId) {
        this.cliId = cliId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Licence licence = (Licence) o;
        return Objects.equals(lic_id, licence.lic_id) &&
                Objects.equals(sale_id, licence.sale_id) &&
                Objects.equals(licence_key, licence.licence_key) &&
                Objects.equals(emission_date, licence.emission_date) &&
                Objects.equals(end_date, licence.end_date) &&
                Objects.equals(cliId, licence.cliId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lic_id, sale_id, licence_key, emission_date, end_date, cliId);
    }

    @Override
    public String toString() {
        return "License{" +
                "lic_id=" + lic_id +
                ", sale_id=" + sale_id +
                ", licence_key=" + licence_key +
                ", emission_date=" + emission_date +
                ", end_date=" + end_date +
                ", cli_id=" + cliId +
                '}';
    }
}

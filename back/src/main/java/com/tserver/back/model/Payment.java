package com.tserver.back.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class Payment {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private
    Long pay_id;

    private LocalDate emission_date;
    private String price;
    private Boolean accepted;
    private LocalDate accepted_date;

    @Override
    public String toString() {
        return "Payment{" +
                "pay_id=" + this.pay_id +
                ", emission_date=" + this.emission_date +
                ", price='" + this.price + '\'' +
                ", accepted=" + this.accepted +
                ", accepted_date=" + this.accepted_date +
                '}';
    }

    public Long getPay_id() {
        return this.pay_id;
    }

    public void setPay_id(final Long pay_id) {
        this.pay_id = pay_id;
    }

    public LocalDate getEmission_date() {
        return this.emission_date;
    }

    public void setEmission_date(final LocalDate emission_date) {
        this.emission_date = emission_date;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(final String price) {
        this.price = price;
    }

    public Boolean getAccepted() {
        return this.accepted;
    }

    public void setAccepted(final Boolean accepted) {
        this.accepted = accepted;
    }

    public LocalDate getAccepted_date() {
        return this.accepted_date;
    }

    public void setAccepted_date(final LocalDate accepted_date) {
        this.accepted_date = accepted_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return Objects.equals(pay_id, payment.pay_id) &&
                Objects.equals(emission_date, payment.emission_date) &&
                Objects.equals(price, payment.price) &&
                Objects.equals(accepted, payment.accepted) &&
                Objects.equals(accepted_date, payment.accepted_date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pay_id, emission_date, price, accepted, accepted_date);
    }
}

package com.tserver.back.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private
    Long prod_id;

    private String name;
    private String price;
    private String description;

    @Override
    public String toString() {
        return "Product{" +
                "prod_id=" + this.prod_id +
                ", name='" + this.name + '\'' +
                ", price='" + this.price + '\'' +
                ", description='" + this.description + '\'' +
                '}';
    }

    public Long getProd_id() {
        return this.prod_id;
    }

    public void setProd_id(final Long prod_id) {
        this.prod_id = prod_id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(final String price) {
        this.price = price;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(prod_id, product.prod_id) &&
                Objects.equals(name, product.name) &&
                Objects.equals(price, product.price) &&
                Objects.equals(description, product.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(prod_id, name, price, description);
    }
}

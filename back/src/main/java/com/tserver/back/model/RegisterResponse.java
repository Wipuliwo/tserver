package com.tserver.back.model;

import org.springframework.http.HttpStatus;

public class RegisterResponse {
    private final HttpStatus httpStatus;
    private final String body;

    public RegisterResponse(HttpStatus httpStatus, String body) {
        this.httpStatus = httpStatus;
        this.body = body;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getBody() {
        return body;
    }
}

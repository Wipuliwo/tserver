package com.tserver.back.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class Sale {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long sale_id;

    private Integer cli_id;
    private Integer pay_id;
    private Integer prod_id;
    private String order_number;
    private String price;
    private LocalDate order_date;
    private Boolean order_completed;
    private LocalDate order_completed_date;

    @Override
    public String toString() {
        return "Sale{" +
                "sale_id=" + this.sale_id +
                ", cli_id=" + this.cli_id +
                ", pay_id=" + this.pay_id +
                ", prod_id=" + this.prod_id +
                ", order_number='" + this.order_number + '\'' +
                ", price='" + this.price + '\'' +
                ", order_date=" + this.order_date +
                ", order_completed=" + this.order_completed +
                ", order_completed_date=" + this.order_completed_date +
                '}';
    }

    public Long getSale_id() {
        return this.sale_id;
    }

    public void setSale_id(final Long sale_id) {
        this.sale_id = sale_id;
    }

    public Integer getCli_id() {
        return this.cli_id;
    }

    public void setCli_id(final Integer cli_id) {
        this.cli_id = cli_id;
    }

    public Integer getPay_id() {
        return this.pay_id;
    }

    public void setPay_id(final Integer pay_id) {
        this.pay_id = pay_id;
    }

    public Integer getProd_id() {
        return this.prod_id;
    }

    public void setProd_id(final Integer prod_id) {
        this.prod_id = prod_id;
    }

    public String getOrder_number() {
        return this.order_number;
    }

    public void setOrder_number(final String order_number) {
        this.order_number = order_number;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(final String price) {
        this.price = price;
    }

    public LocalDate getOrder_date() {
        return this.order_date;
    }

    public void setOrder_date(final LocalDate order_date) {
        this.order_date = order_date;
    }

    public Boolean getOrder_completed() {
        return this.order_completed;
    }

    public void setOrder_completed(final Boolean order_completed) {
        this.order_completed = order_completed;
    }

    public LocalDate getOrder_completed_date() {
        return this.order_completed_date;
    }

    public void setOrder_completed_date(final LocalDate order_completed_date) {
        this.order_completed_date = order_completed_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sale sale = (Sale) o;
        return Objects.equals(sale_id, sale.sale_id) &&
                Objects.equals(cli_id, sale.cli_id) &&
                Objects.equals(pay_id, sale.pay_id) &&
                Objects.equals(prod_id, sale.prod_id) &&
                Objects.equals(order_number, sale.order_number) &&
                Objects.equals(price, sale.price) &&
                Objects.equals(order_date, sale.order_date) &&
                Objects.equals(order_completed, sale.order_completed) &&
                Objects.equals(order_completed_date, sale.order_completed_date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sale_id, cli_id, pay_id, prod_id, order_number, price, order_date, order_completed, order_completed_date);
    }
}

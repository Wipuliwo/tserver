package com.tserver.back.repository;

import com.tserver.back.model.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {

    Optional<Client> findByUsername(String userName);

    Optional<Client> findByEmail(String email);
}

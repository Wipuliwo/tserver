package com.tserver.back.repository;

import com.tserver.back.model.Licence;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LicenceRepository extends CrudRepository<Licence, Long> {

    Iterable<Licence> findByCliId(Long id);
}

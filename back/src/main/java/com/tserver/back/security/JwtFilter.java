package com.tserver.back.security;

import com.tserver.back.model.UserDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtFilter extends OncePerRequestFilter {

    @Autowired
    UserDetailService userDetailService;
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String username = null;
        String jwt = null;

        String auth = httpServletRequest.getHeader("Authorization");
        if(auth != null && auth.startsWith("Bearer ")){
            jwt = auth.substring(7);
            username = JwtUtils.getUserFromToken(jwt);
        }

        //notice the null comparison as we verify here if we are in a stateless context configuration otherwise it wont try to authenticate the request
        if(username != null && SecurityContextHolder.getContext().getAuthentication() == null){
            UserDetails userDetail = userDetailService.loadUserByUsername(username);
            if(username != null && userDetail.isAccountNonExpired() && JwtUtils.validateJwt(userDetail, jwt)){
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetail, null, userDetail.getAuthorities());
                authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}

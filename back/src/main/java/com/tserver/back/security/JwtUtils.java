package com.tserver.back.security;

import com.tserver.back.model.UserDetail;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.userdetails.UserDetails;

import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class JwtUtils {

    private static final String SECRET = "shhhh";

    public static String getJwtToken(UserDetail userDetail){
        String userName = userDetail.getUsername();
        return generateToken(new HashMap<>(), userName);
    }

    public static Boolean validateJwt(UserDetails userDetail, String token){
        return getUserFromToken(token).equals(userDetail.getUsername()) && isJwtNotExpired(token);
    }

    public static String getUserFromToken(String token){
        return extractAllClaims(token).getSubject();
    }

    private static String generateToken(Map<String, Object> claims, String userName){
        return Jwts.builder()
                .addClaims(claims)
                .setSubject(userName)
               .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .compact();
    }

    private static Boolean isJwtNotExpired(String token){
        Claims claims = extractAllClaims(token);
        return claims.getExpiration().after(new Date(System.currentTimeMillis()));
    }

    private static Claims extractAllClaims(String token){
        return Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
    }
}

package com.tserver.back.security;

import com.tserver.back.model.Client;
import com.tserver.back.model.Licence;
import com.tserver.back.model.UserDetail;
import com.tserver.back.repository.ClientRepository;
import com.tserver.back.repository.LicenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.StreamSupport;

@Service
public class UserDetailService implements UserDetailsService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private LicenceRepository licenseRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Client client = findClientByUsername(s);
        Licence licence = findLatestLicense(client);

        return buildUserDetail(client, licence);
    }

    private Licence findLatestLicense(Client client) {
        Iterable<Licence> allUserLicenses = licenseRepository.findByCliId(client.getCli_id());
        return StreamSupport.stream(allUserLicenses.spliterator(), false)
                .max(Comparator.comparing(Licence::getEnd_date)).get();
    }

    private Client findClientByUsername(String userName) {
        return clientRepository.findByUsername(userName).orElse(null);
    }

    private UserDetails buildUserDetail(Client client, Licence licence){
        final UserDetail userDetail = new UserDetail();
        userDetail.setPassword(passwordEncoder.encode(client.getPassword()));
        userDetail.setUsername(client.getUsername());
        userDetail.setAccountNonExpired(isLicenseExpired(licence));
        userDetail.setAuthorityList(Arrays.asList(new SimpleGrantedAuthority("client")));

        return userDetail;
    }

    private boolean isLicenseExpired(Licence licence) {
        return licence.getEnd_date().isBefore(LocalDate.now());
    }


}

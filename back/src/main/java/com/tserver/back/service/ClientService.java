package com.tserver.back.service;

import com.tserver.back.model.Client;
import com.tserver.back.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class ClientService {

    @Autowired
    ClientRepository clientRepository;

    public Client getClientByUsername(String username) throws NoSuchElementException {
        return clientRepository.findByUsername(username).orElseThrow(NoSuchElementException::new);
    }


    public Client editClientData(Client client, String userName) throws NoSuchElementException {
         Client oldClient = clientRepository.findByUsername(userName).orElseThrow(NoSuchElementException::new);
         Client updatedClient = getUpdatedClient(oldClient, client);
         return clientRepository.save(updatedClient);
    }

    private Client getUpdatedClient(Client oldClient, Client newClient)  {
        oldClient.setCity(nullProof(newClient.getCity(), oldClient.getCity()));
        oldClient.setCountry(nullProof(newClient.getCountry(), oldClient.getCountry()));
        oldClient.setAddress(nullProof(newClient.getAddress(), oldClient.getAddress()));
        oldClient.setEmail(nullProof(newClient.getEmail(), oldClient.getEmail()));
        oldClient.setPassword(nullProof(newClient.getPassword(), oldClient.getPassword()));
        oldClient.setFirst_name(nullProof(newClient.getFirst_name(), oldClient.getFirst_name()));
        oldClient.setLast_name(nullProof(newClient.getLast_name(), oldClient.getLast_name()));

        return oldClient;
    }

    private <T> T nullProof( T newField, T oldField){
        return newField == null ? oldField : newField;
    }
}

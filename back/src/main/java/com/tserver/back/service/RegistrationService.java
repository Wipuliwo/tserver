package com.tserver.back.service;

import com.tserver.back.controllers.factories.LicenceFactory;
import com.tserver.back.exceptions.FieldAlreadyExistsException;
import com.tserver.back.model.Client;
import com.tserver.back.model.Licence;
import com.tserver.back.model.RegisterResponse;
import com.tserver.back.repository.ClientRepository;
import com.tserver.back.repository.LicenceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDate;

@Service
public class RegistrationService {

    private final Logger logger = LoggerFactory.getLogger(RegistrationService.class);

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    LicenceRepository licenseRepository;

    @Transactional
    public RegisterResponse registerClient(Client client)  {
        final String username = client.getUsername();
        final String email = client.getEmail();

        try{
            verifyUserNameExists(username);
            verifyEmailExists(email);
            Client savedClient = clientRepository.save(client);
            registerNewClientLicense(savedClient.getCli_id());

        }catch (FieldAlreadyExistsException ex){
            return new RegisterResponse(HttpStatus.CONFLICT, ex.getMessage());
        }catch (DataIntegrityViolationException ex){
            return new RegisterResponse(HttpStatus.PRECONDITION_FAILED, "Incorrect Field");
        }

        return new RegisterResponse(HttpStatus.OK, "");
    }

    private void verifyUserNameExists(String userName) throws FieldAlreadyExistsException {
        if(clientRepository.findByUsername(userName).isPresent()){
            throw new FieldAlreadyExistsException("username already registered");
        }
    }

    private void verifyEmailExists(String email) throws FieldAlreadyExistsException{
        if(clientRepository.findByEmail(email).isPresent()){
            throw new FieldAlreadyExistsException("email already registered");
        }
    }

    private void registerNewClientLicense(Long clientId){
        Licence license = LicenceFactory.getInstance().getLicense(clientId);
        license.setEnd_date(LocalDate.now().plusDays(5));
        licenseRepository.save(license);
    }


}

DROP table if exists client cascade;
CREATE TABLE client (
	cli_id BIGSERIAL UNIQUE NOT NULL,

	first_name varchar(50) NOT NULL,
	last_name varchar(50) NOT NULL,
	username varchar(50) UNIQUE NOT NULL,
	password varchar(50) NOT NULL,
	email varchar(50) UNIQUE NOT NULL,
	emailv boolean NOT NULL,
	verification_code varchar(50) NULL UNIQUE,
	address varchar(50) NULL,
	city varchar(50) NULL,
	zip_code varchar(50) NULL,
	country varchar(50) NULL,
	creation_date timestamp NULL
);
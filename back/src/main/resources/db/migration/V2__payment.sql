DROP table if exists payment cascade;
CREATE TABLE payment (
	pay_id BIGSERIAL NOT NULL,

	emission_date timestamp NULL,
	price varchar(255) NULL,
	accepted boolean NOT NULL,
	accepted_date timestamp NULL
);

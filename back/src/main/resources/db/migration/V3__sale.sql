DROP table if exists sale cascade;
CREATE TABLE sale (
	sale_id BIGSERIAL NOT NULL,

	cli_id int8 NULL,
	pay_id int8 NULL,
	prod_id int8 NULL,
	order_number varchar(255) NULL,
	price varchar(255) NULL,
	order_date timestamp NULL,
	order_completed boolean NOT NULL,
	order_completed_date timestamp NULL
);
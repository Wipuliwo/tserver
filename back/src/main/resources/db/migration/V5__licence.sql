CREATE TABLE licence (
	lic_id BIGSERIAL NOT NULL,

    cli_id BIGSERIAL NOT NULL,
	sale_id int8 NULL,
	licence_key int8 NULL UNIQUE,
	emission_date timestamp NULL,
	end_date timestamp NULL
);
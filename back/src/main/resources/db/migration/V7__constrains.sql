ALTER TABLE client ADD PRIMARY KEY (cli_id);
ALTER TABLE payment ADD PRIMARY KEY (pay_id);
ALTER TABLE sale ADD PRIMARY KEY (sale_id);
ALTER TABLE product ADD PRIMARY KEY (prod_id);
ALTER TABLE licence ADD PRIMARY KEY  (lic_id);

ALTER TABLE sale ADD FOREIGN KEY (cli_id) REFERENCES client(cli_id);
ALTER TABLE sale ADD FOREIGN KEY (prod_id) REFERENCES product(prod_id);
ALTER TABLE sale ADD FOREIGN KEY  (pay_id) REFERENCES payment(pay_id);
ALTER TABLE licence ADD FOREIGN KEY (sale_id) REFERENCES sale(sale_id);
ALTER TABLE licence ADD FOREIGN KEY (cli_id) REFERENCES client(cli_id);
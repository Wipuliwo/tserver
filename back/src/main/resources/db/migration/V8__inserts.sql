INSERT INTO client (cli_id,first_name,last_name,username,password,email,emailv,verification_code,address,city,zip_code,country,creation_date) VALUES
(1,'John','Doe','test','pass','johnDoe@gmail.com',TRUE,'AAABBBCCC0213','Narrow Street','Paradise City','1234','Zoltan','2019-04-09 16:42:59.918');

INSERT INTO product (prod_id,name,price,description) VALUES
(1,'Licence Key - 30 Day','5','Full Access to our tool for 30 Days.');

INSERT INTO payment (pay_id,emission_date,price,accepted,accepted_date) VALUES
(1,'2019-04-09 17:42:59.918','5',TRUE,'2019-04-09 17:43:59.918');

INSERT INTO sale (sale_id,cli_id,pay_id,prod_id,order_number,price,order_date,order_completed,order_completed_date) VALUES
(1,1,1,1,'00001','5','2019-04-09 17:42:59.918',TRUE,'2019-04-09 17:44:59.918');

INSERT INTO licence (lic_id,cli_id,sale_id,licence_key,emission_date,end_date) VALUES
(1,1,1,0123456789,'2019-04-09 17:42:59.918','2020-05-09 17:44:59.918');




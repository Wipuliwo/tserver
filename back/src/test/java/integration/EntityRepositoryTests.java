package integration;

import com.tserver.back.ServerApplication;
import com.tserver.back.model.*;
import com.tserver.back.repository.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ServerApplication.class)
public class EntityRepositoryTests {

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    SaleRepository saleRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    LicenceRepository licenseRepository;

    @Test
    public void givenEntityRepository_whenSaveAndRetreiveEntity_thenOK(){

        clientRepository.save(new Client());
        productRepository.save(new Product());
        saleRepository.save(new Sale());
        paymentRepository.save(new Payment());
        licenseRepository.save(new Licence());

        Client foundClient = clientRepository.findById(1L).get();
        Product foundProduct = productRepository.findById(1L).get();
        Sale foundSale = saleRepository.findById(1L).get();
        Payment foundPayment = paymentRepository.findById(1L).get();
        Licence foundLicence = licenseRepository.findById(1L).get();

        assertNotNull(foundClient);
        assertNotNull(foundProduct);
        assertNotNull(foundSale);
        assertNotNull(foundPayment);
        assertNotNull(foundLicence);

    }
}

package model;

import com.tserver.back.model.*;
import org.junit.Test;

import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;

public class PojoTests {

    @Test
    public void test_all_pojos(){

        final Class<?> client = Client.class;
        final Class<?> license = Licence.class;
        final Class<?> payment = Payment.class;
        final Class<?> sale = Sale.class;
        final Class<?> product = Product.class;

        assertPojoMethodsFor(client).areWellImplemented();
        assertPojoMethodsFor(license).areWellImplemented();
        assertPojoMethodsFor(payment).areWellImplemented();
        assertPojoMethodsFor(sale).areWellImplemented();
        assertPojoMethodsFor(product).areWellImplemented();

    }

}

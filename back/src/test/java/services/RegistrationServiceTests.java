package services;

import com.tserver.back.ServerApplication;
import com.tserver.back.model.Client;
import com.tserver.back.model.Licence;
import com.tserver.back.model.RegisterResponse;
import com.tserver.back.repository.ClientRepository;
import com.tserver.back.repository.LicenceRepository;
import com.tserver.back.service.RegistrationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Optional;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ServerApplication.class)
@Transactional
public class RegistrationServiceTests {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private LicenceRepository licenseRepository;

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private EntityManager entityManager;


    @Before
    public void setup(){
    }

    @Test
    public void simpleUserRegistration_HappyPath(){
        Client client = setupValidClient();
        RegisterResponse result = registrationService.registerClient(client);

        Optional<Client> savedClient = clientRepository.findByUsername(client.getUsername());
        Iterable<Licence> licence = licenseRepository.findByCliId(savedClient.get().getCli_id());

        assertTrue(savedClient.isPresent());
        assertTrue(licence.iterator().hasNext());
        assertTrue(result.getHttpStatus() == HttpStatus.OK);
    }

    @Test
    public void UserAlreadyRegistered_returnsExpected(){
        registrationService.registerClient(setupValidClient());

        RegisterResponse result = registrationService.registerClient(setupValidClient());

        assertTrue(result.getHttpStatus() == HttpStatus.CONFLICT);
    }

    @Test
    public void missingUsernameRegistrationValue_returnsExpectedStatus_andDoesNotRegisterClientInDB(){
        Client client = setupInvalidClient();
        client.setEmail("yolo@gmail.com");

        RegisterResponse result = registrationService.registerClient(client);
        entityManager.clear();
        Optional<Client> optionalClient = clientRepository.findByEmail(client.getEmail());

        assertTrue(result.getHttpStatus() == HttpStatus.PRECONDITION_FAILED);
        assertTrue(!optionalClient.isPresent());
    }

    @Test
    public void missingEmailRegistrationValue_returnsExpectedStatus_andDoesNotRegisterClientInDB(){
        Client client = setupInvalidClient();
        client.setUsername("yolo");

        RegisterResponse result = registrationService.registerClient(client);
        entityManager.clear();
        Optional<Client> optionalClient = clientRepository.findByUsername(client.getUsername());

        assertTrue(result.getHttpStatus() == HttpStatus.PRECONDITION_FAILED);
        assertTrue(!optionalClient.isPresent());
    }

    @Test
    public void fieldTooLong_returnsExpectedStatus_andDoesNotRegisterClientInDB(){
        Client client = setupValidClient();
        client.setLast_name("123456789023456789012345678902345678901234567890959");

        RegisterResponse result = registrationService.registerClient(client);
        entityManager.clear();
        Optional<Client> optionalClient = clientRepository.findByUsername(client.getUsername());

        assertTrue(result.getHttpStatus() == HttpStatus.PRECONDITION_FAILED);
        assertTrue(!optionalClient.isPresent());
    }

    private Client setupValidClient(){
        final Client client = new Client();
        client.setFirst_name("John");
        client.setLast_name("Doe");
        client.setEmail("test9912286@mail.com");
        client.setUsername("tester9912286");
        client.setPassword("test");

        return client;
    }

    private Client setupInvalidClient(){
        return new Client();
    }

}
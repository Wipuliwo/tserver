import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './screens/home/home.component';
import { PricingComponent } from './screens/pricing/pricing.component';
import { AccountComponent } from './screens/account/account.component';
import { HelpComponent } from './screens/help/help.component';
import { ContactsComponent } from './screens/contacts/contacts.component';
import { ForumComponent } from './screens/forum/forum.component';
import { LoginComponent } from './screens/login/login.component';
import { RegisterComponent } from './screens/register/register.component';

const routes: Routes = [
    { path: 'pricing' , component: PricingComponent},
    { path: '', component: HomeComponent},
    { path: 'account', component: AccountComponent},
    { path: 'help', component: HelpComponent},
    { path: 'contacts', component: ContactsComponent},
    { path: 'forum', component: ForumComponent},
    { path: 'login', component: LoginComponent},
    { path: 'register', component: RegisterComponent},
];

@NgModule({
imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule {}
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFullpageModule } from '@fullpage/angular-fullpage';
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component';
import { PricingComponent } from './screens/pricing/pricing.component';
import { HomeComponent } from './screens/home/home.component';
import { ContactsComponent } from './screens/contacts/contacts.component';
import { AccountComponent } from './screens/account/account.component';
import { HelpComponent } from './screens/help/help.component';
import { ForumComponent } from './screens/forum/forum.component';
import { LoginComponent } from './screens/login/login.component';
import { RegisterComponent } from './screens/register/register.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    PricingComponent,
    HomeComponent,
    ContactsComponent,
    AccountComponent,
    HelpComponent,
    ForumComponent,
    LoginComponent,
    RegisterComponent,
    AccountComponent
  ],
  imports: [
    BrowserModule,
    AngularFullpageModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../services/account/account.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../models/user';
import { first, throwIfEmpty } from 'rxjs/operators';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  
  accountDetails : FormGroup;
  clientName = 'Tester';
  licenceDays = 5;
  user : User;
  

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private router: Router
  ){
    this.accountDetails = this.formBuilder.group({
      firstname: [''],
      lastname: [''],
      email: [''],
      password: [''],
      address: [''],
      country: [''],
      city: ['']
    })
  }


  ngOnInit() {
    
    this.loadUser();
    this.loadAccountDetails();

  }

  // convenience getter for easy access to form fields
  get f() { return this.accountDetails.controls; }

  private loadUser() {
    this.accountService.getLoggedInUser()
        .subscribe(
          data => {
            this.user = data;
            this.loadAccountDetails();
          },
          error => {
            console.log("error with token: " + localStorage.getItem('token')) 
            this.router.navigate(['/login']);
          });

  }

  private loadAccountDetails(){
    this.f['firstname'].setValue(this.user.first_name);
    this.f['lastname'].setValue(this.user.last_name);
    this.f['email'].setValue(this.user.email);
    this.f['password'].setValue(this.user.password);
    this.f['address'].setValue(this.user.address);
    this.f['country'].setValue(this.user.country);
    this.f['city'].setValue(this.user.city);
    }

  public handleChangeClick(someElement){
    const input = document.getElementById(someElement) as HTMLInputElement;;
    input.disabled = false;
    input.focus();
    input.style.color = 'red';
  }

  
  onSubmit(){
    console.log("submit");
    this.accountService.editUser(this.accountDetails.value).subscribe(
      data => {
        this.user = data;
        window.location.reload();
      },
      error => {
        console.log("error with token: " + localStorage.getItem('token')) 
        this.router.navigate(['/login']);
      });
  }

}

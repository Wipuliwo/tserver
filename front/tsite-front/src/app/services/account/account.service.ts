import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../../models/user';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private client : HttpClient) {}

  public registerUser(user: User){return this.client.post("http://localhost:8082/api/register",user,{observe: 'response'})}

  public getLoggedInUser() : Observable<User>{
    const requestheaders = this.getAccessTokenHeader();
    console.log(requestheaders)
    return this.client.get<User>("http://localhost:8082/api/client/", {headers : requestheaders})}

  public editUser(user: User){return this.client.put<User>("http://localhost:8082/api/client/",user,{headers: this.getAccessTokenHeader()})}

  private getAccessTokenHeader(){
    return new HttpHeaders().set("Authorization", localStorage.getItem('token'))
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private client : HttpClient) { }

  public authenticate(username, password){
    const headers = this.getBasicAuthHeaders(username, password);
    return this.getAccessToken(headers);
  }

  private getAccessToken(headers){
    return this.client.get("http://localhost:8082/auth",{headers, responseType: 'text'})
  }

  private getBasicAuthHeaders(username, password){
    let auth ="Basic " + btoa(username + ":" + password);
    return new HttpHeaders().set("Authorization", auth)
  }
}
